package com.jamk.mobilegroup.tiquet.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jamk.mobilegroup.tiquet.MainActivity;
import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.adapters.DraftAdapter;
import com.jamk.mobilegroup.tiquet.adapters.ReceivedAdapter;
import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;

import java.util.List;

import javax.annotation.Nullable;

import io.realm.RealmResults;

/**
 * Created by Aaro on 27.11.2017.
 */

public class ReceivedFragment extends Fragment {
    public static final String TITLE = "Received";

    private ReceiptRepository receiptRepository;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ReceivedAdapter mAdapter;

    public static ReceivedFragment newInstance() {
        return new ReceivedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiptRepository = new ReceiptRepository();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        List<Receipt> receipts = receiptRepository.RetrieveByStatus(3, MainActivity.User, "user");

        View v = inflater.inflate(R.layout.fragment_received, null);

        final RecyclerView text = (RecyclerView) v.findViewById(R.id.rv_received_receipts);

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Testi", Toast.LENGTH_SHORT).show();
            }
        });

        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_received_receipts);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ReceivedAdapter(receipts);

        mRecyclerView.setAdapter(mAdapter);

        return v;
    }

}
