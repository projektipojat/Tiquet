package com.jamk.mobilegroup.tiquet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

import com.jamk.mobilegroup.tiquet.MainActivity;
import com.jamk.mobilegroup.tiquet.R;
import com.jamk.mobilegroup.tiquet.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Veeti on 6.11.2017.
 */

public class CreateReceiptDialogFragment extends DialogFragment {

    public interface ReceiptDialogListener {
        void doPositiveClick(DialogFragment dialog, JSONObject receipt);
        void doNegativeClick(DialogFragment dialog);
    }
    ReceiptDialogListener mListener;
    JSONObject json;
    private String user;
    private UserRepository userRepository;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userRepository = new UserRepository();

        user = getArguments() != null ? getArguments().getString("user") : null;
        user = userRepository.GetNameByID(user);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ReceiptDialogListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ReceiptDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_new_receipt_form, null))
                .setPositiveButton(R.string.create_form_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {

                                Dialog f = (Dialog) dialog;
                                json = new JSONObject();
                                EditText product, manufacturer, model, serial, description, price;
                                product = (EditText)f.findViewById(R.id.et_receipt_product_name);
                                manufacturer = (EditText)f.findViewById(R.id.et_receipt_manufacturer);
                                model = (EditText)f.findViewById(R.id.et_receipt_model);
                                serial = (EditText)f.findViewById(R.id.et_receipt_serial);
                                description = (EditText)f.findViewById(R.id.et_receipt_description);
                                price = (EditText)f.findViewById(R.id.et_receipt_price);

                                if (verifyFields(f)) {
                                    try{
                                        json.put("product", product.getText().toString());
                                        json.put("manufacturer", manufacturer.getText().toString());
                                        json.put("model", model.getText().toString());
                                        json.put("serial", serial.getText().toString());
                                        json.put("description", description.getText().toString());
                                        json.put("price", Double.parseDouble(price.getText().toString()));
                                        json.put("creator", user);
                                        mListener.doPositiveClick(CreateReceiptDialogFragment.this, json);
                                    }
                                    catch (JSONException e){}
                                }
                                else {
                                    Toast.makeText(getContext(), "Error while verifying input fields, receipt was not created.", Toast.LENGTH_SHORT).show();
                                }
                            }

                        })
                .setNegativeButton(R.string.create_form_cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                CreateReceiptDialogFragment.this.getDialog().cancel();
                                mListener.doNegativeClick(CreateReceiptDialogFragment.this);
                            }
                        });
        return builder.create();
    }

    private boolean verifyFields(Dialog dialog){
        EditText product, manufacturer, model, serial, description, price;
        product = (EditText)dialog.findViewById(R.id.et_receipt_product_name);
        manufacturer = (EditText)dialog.findViewById(R.id.et_receipt_manufacturer);
        model = (EditText)dialog.findViewById(R.id.et_receipt_model);
        serial = (EditText)dialog.findViewById(R.id.et_receipt_serial);
        description = (EditText)dialog.findViewById(R.id.et_receipt_description);
        price = (EditText)dialog.findViewById(R.id.et_receipt_price);

        try {
            double priceCheck = Double.parseDouble(price.getText().toString());
        }
        catch (NumberFormatException e) {
            return false;
        }

        return !product.getText().toString().isEmpty() &&
                !manufacturer.getText().toString().isEmpty() &&
                !model.getText().toString().isEmpty() && !serial.getText().toString().isEmpty() &&
                !serial.getText().toString().isEmpty() &&
                !description.getText().toString().isEmpty() &&
                !price.getText().toString().isEmpty();
    }
}
