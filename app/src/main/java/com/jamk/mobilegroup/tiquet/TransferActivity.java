package com.jamk.mobilegroup.tiquet;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.jamk.mobilegroup.tiquet.model.Receipt;
import com.jamk.mobilegroup.tiquet.repository.ReceiptRepository;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.Date;

import io.realm.RealmResults;

public class TransferActivity extends AppCompatActivity implements
        NfcAdapter.CreateNdefMessageCallback{
    private ReceiptRepository receiptRepository;
    private NfcAdapter mNfcAdapter;
    private String strReceipt;
    private TextView tvProduct, tvManufacturer, tvModel, tvSerial, tvDescription, tvPrice, tvCreator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        receiptRepository = new ReceiptRepository();

        Intent intent = getIntent();
        String id = intent.getStringExtra(MainActivity.RECEIPT_ID);

        RealmResults<Receipt> result = receiptRepository.FindById(id);
        this.strReceipt = result.first().getReceipt().toString();

        tvProduct = (TextView) findViewById(R.id.tv_transfer_product);
        tvManufacturer = (TextView) findViewById(R.id.tv_transfer_manufacturer);
        tvModel = (TextView) findViewById(R.id.tv_transfer_model);
        tvSerial = (TextView) findViewById(R.id.tv_transfer_serial);
        tvDescription = (TextView) findViewById(R.id.tv_transfer_description);
        tvPrice = (TextView) findViewById(R.id.tv_transfer_price);
        tvCreator = (TextView) findViewById(R.id.tv_transfer_creator);


        try {
            JSONObject json = new JSONObject(strReceipt);
            tvProduct.setText("Product : " + json.get("product").toString());
            tvManufacturer.setText("Manufacturer : " + json.get("manufacturer").toString());
            tvModel.setText("Model : " + json.get("model").toString());
            tvSerial.setText("Serial : " + json.get("serial").toString());
            tvDescription.setText("Description : " + json.get("description").toString());
            tvPrice.setText("Price : " + json.get("price").toString());
            tvCreator.setText("Creator : " + json.get("creator").toString());
        }
        catch(JSONException e){
            e.printStackTrace();
        }
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mNfcAdapter.setNdefPushMessageCallback(this, this);

        setBeamCompleteCallback(this, new NfcAdapter.OnNdefPushCompleteCallback() {
            @Override
            public void onNdefPushComplete(NfcEvent event) {
                NfcCompleted();
            }
        });
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        RealmResults<Receipt> result = receiptRepository.FindByReceipt(strReceipt);
        try {
            JSONObject receipt = new JSONObject(strReceipt);
            String currentTime = DateFormat.getDateTimeInstance().format(new Date());
            receipt.put("createtime",currentTime);
            strReceipt = receipt.toString();
        }
        catch (JSONException e) {

        }
        receiptRepository.UpdateFields(result.first(), strReceipt);

        return new NdefMessage(new NdefRecord[]{
                NdefRecord.createMime(
                        "application/com.jamk.mobilegroup.tiquet", strReceipt.getBytes())
        });
    }

    public void setBeamCompleteCallback(Activity activity, NfcAdapter.OnNdefPushCompleteCallback callback) {
        mNfcAdapter.setOnNdefPushCompleteCallback(callback, activity);
    }

    public void NfcCompleted() {
        this.finish();
    }
}
