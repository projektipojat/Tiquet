package com.jamk.mobilegroup.tiquet.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import com.jamk.mobilegroup.tiquet.R;

/**
 * Created by Veeti on 17.11.2017.
 */

public class NoNFCReceiptDialogFragment extends DialogFragment {

    public interface NoNFCListener {
        void doPositiveClick(DialogFragment dialog);
    }

    NoNFCListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (NoNFCListener) activity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ReceiptDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.no_nfc_title)
                .setMessage(R.string.no_nfc_message)
                .setPositiveButton(R.string.no_nfc_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.doPositiveClick(NoNFCReceiptDialogFragment.this);
                    }
                });
        return builder.create();
    }
}
