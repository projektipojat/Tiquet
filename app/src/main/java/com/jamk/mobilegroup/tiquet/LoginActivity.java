package com.jamk.mobilegroup.tiquet;

import android.app.DialogFragment;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jamk.mobilegroup.tiquet.fragments.CreateReceiptDialogFragment;
import com.jamk.mobilegroup.tiquet.fragments.CreateUserDialogFragment;
import com.jamk.mobilegroup.tiquet.fragments.NoNFCReceiptDialogFragment;
import com.jamk.mobilegroup.tiquet.model.User;
import com.jamk.mobilegroup.tiquet.repository.UserRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class LoginActivity extends AppCompatActivity implements CreateUserDialogFragment.RegisterDialogListener, NoNFCReceiptDialogFragment.NoNFCListener {
    public NfcAdapter mNfcAdapter;
    public static final String USER_ID = "com.jamk.mobilegroup.tiquet.USER";
    private String strUser;
    private UserRepository userRepository;
    private File loggedUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Realm.init(this);

        userRepository = new UserRepository();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkNfc();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void goToMainActivity(View view) {
        EditText logusername = (EditText) findViewById(R.id.UsernameLogin);
        EditText logpassword = (EditText) findViewById(R.id.PasswordLogin);

        String user = userRepository.CheckUser(logusername.getText().toString(), logpassword.getText().toString());

        if (user != null) {
            loggedUser = new File(getApplicationContext().getFilesDir(), "logged_user.txt");
            FileOutputStream outputStream;
            try {
                outputStream = openFileOutput("logged_user.txt", getApplicationContext().MODE_PRIVATE);
                outputStream.write(user.getBytes());
                outputStream.close();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(USER_ID, user);
            startActivity(intent);
        }
        else {
            Toast.makeText(getApplicationContext(), "Username or Password is wrong", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkNfc() {
        if(mNfcAdapter == null) {
            NoNFCReceiptDialogFragment dialog = new NoNFCReceiptDialogFragment();
            dialog.show(getFragmentManager(), "No support");
        }
    }
    public void openCreateUserDialog(View view) {
        CreateUserDialogFragment dialog = new CreateUserDialogFragment();
        dialog.show(getFragmentManager(), "openDialog");
    }

    @Override
    public void doPositiveClick(DialogFragment dialog) {
    }

    @Override
    public void doPositiveClick(DialogFragment dialog, RealmObject user) {
        strUser = user.toString();
        Toast.makeText(getApplicationContext(), strUser.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void doNegativeClick(DialogFragment dialog) {
        Toast.makeText(getApplicationContext(), "Cancel clicked", Toast.LENGTH_LONG).show();

    }
}
